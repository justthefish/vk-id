<?php
/**
 *
 * Читает csv файл, где в первой колонке screen_name аккаунтов людей или групп VK.com
 * после чего создает новый файл на основе первого, но добавляет после первой колоки 
 * две дополнительные:
 * - Является ли группой (bool)
 * - id группы или человека (int)
 */
if (!isset($argv[1])) {
    die("usage: php5 '.$argv[0].' <csv file>\n\n");
}

const OUTPUT_CSV_FILE = 'output.csv';
const CSV_DELIMITER = ';';

require_once('./vk-oauth/src/VK/VK.php');
require_once('./vk-oauth/src/VK/VKException.php');

$vk_config = array(
    'app_id'        => '4644071',
    'api_secret'    => 'VaO77MS8EjsgJoiDiwST',
    'access_token'  => '453f3f27cb8a802367f6074fa935bf9db268faa13f5035bcf2a82f92dc33750f527d3dbc66f07ac8eb1e7b01f8635'
);

$vk = new VK\VK($vk_config['app_id'], $vk_config['api_secret'], $vk_config['access_token']);
 
$output = array();
$count = 0;
if (($handle = fopen($argv[1], "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, CSV_DELIMITER)) !== FALSE) {
        $count++;
        $is_group = false;
        $screen_name = $data[0];
        $id = intval(str_replace('id', '', $screen_name));
        //Skipping plain ids
        if (!$id) {

            try {
                $response = $vk->api('users.get', array(
                    'user_ids' => $screen_name,
                ));
                if (!isset($response['error'])) {
                    $id = $response['response'][0]['uid'];
                } else {
                    if($response['error']['error_code'] == 113) { //is a group
                        $response = $vk->api('groups.getById', array(
                            'group_id' => $screen_name
                        ));
                        if (isset($response['response']) ) {
                            $is_group = true;
                            $id = $response['response'][0]['gid'];
                        } else {
                           $id = -1; // User or group deleted in VK 
                        }
                    }
                }
                sleep(1); // to prevent flood
            } catch (VKException $e) {
                throw $e;
            } catch (Exception $e) {
                throw $e;
            }
        } 
        $tmp = $data;
        array_splice($tmp, 1, 0, $id); //insert id column after screen_name
        array_splice($tmp, 1, 0, $is_group); //insert id column after screen_name
        $output[] = $tmp;

        if ($count % 500 == 0 ) {
            echo('.');
        }
        if ($count % 2000 == 0) {
            echo "\n";
        }
    }
    fclose($handle);
} else {
    die('Cannot open file '.$argv[1].' for reading');
}
echo "\n\t..Saving output file\n";
if ($fp = fopen(OUTPUT_CSV_FILE, 'w')) {
    foreach($output as $record) {
        fputcsv($fp, $record);
    }
    fclose($fp);
} else {
    die('Cannot open file '.OUTPUT_CSV_FILE.' for writing');
}
